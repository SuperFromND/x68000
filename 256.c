#include <stdio.h>
#include <graph.h>

void color_cycle(){
	int i = 0;
	
	for( i = 0; i < 256; i++ ){
		fill( 0, i*2, 511, (i*2)+1, i );
	}
}

void main(){
	_iocs_crtmod(8); /* 512x512, 256 colors */
	_iocs_g_clr_on();
	color_cycle();
}