# X68000 Experiments

This repository contains various experimental programs written for the Sharp X68000 home computer, released in 1987 by Sharp Corporation.

Programs are provided in both binary (.x) and source (.c) format.

# Compiling
These programs are intended to be compiled using [Neozeed's X68000 GCC compiler for Windows](https://sourceforge.net/projects/gcc-1-30-x68000/):
```
gcc hello.c -o hello.x -ldos -liocs
```

# Running
It's recommended to use [XM6 Pro-68k](http://mijet.eludevisibility.org/XM6%20Pro-68k/XM6%20Pro-68k.html) to run .x files, as they can easily be loaded using the "Floppy Drive -> Insert Directory" feature.

# List of programs

* hello - Prints "Hello X68000!", then exits.
* gradient - Sets the screen to 512x512 @ 256 colors, draws a blue gradient, then exits.
* 256 - Sets the screen to 512x512 @ 256 colors, draws a gradient using the entire 256-color palette, then exits.