#include <stdio.h>
#include <graph.h>

void grad(){
	int i = 0;
	
	for( i = 0; i < 8; i++ ){
		fill( 0, i*64, 511, (i*64) + 64, i );
	}
}

void main(){
	_iocs_crtmod(8); /* 512x512, 256 colors */
	_iocs_g_clr_on();
	grad();
}